module ru.mycompany.pc_checker {
    requires java.sql;
    requires javafx.controls;
    requires javafx.fxml;
    
    requires java.persistence;
    requires java.base;
    requires org.hibernate.orm.core;

    opens ru.mycompany.pc_checker to javafx.fxml;
    exports ru.mycompany.pc_checker;
}
