package ru.mycompany.pc_checker;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import ru.mycompany.pc_checker.dbase.DBHandler;
import ru.mycompany.pc_checker.dbase.User;

public class PersonalLogInScreenController {

    @FXML
    private Button addPersonButton;

    @FXML
    private TextField login_login_field;

    @FXML
    private PasswordField login_password_field;

    @FXML
    private void switchToLoginScreen() throws IOException {
        App.setRoot("loginScreen");
    }

    @FXML
    private void switchToPersRegScreen() throws IOException {
        App.setRoot("personalRegScreen");
    }

    @FXML
    private void logIn() throws IOException, SQLException, ClassNotFoundException {
        String loginText = login_login_field.getText().trim();
        String loginPassword = login_password_field.getText().trim();

        if (!loginText.equals("") && !loginPassword.equals("")) {
            loginUser(loginText, loginPassword);
        } else {
            System.out.println("Empty login and password field");
        }
    }

    private void loginUser(String loginText, String loginPassword) throws SQLException, ClassNotFoundException, IOException {

        DBHandler dbHandler = new DBHandler();

        User user = new User();
        user.setLogin(loginText);
        user.setPassword(loginPassword);

        ResultSet result = dbHandler.getUser(user);

        int counter = 0;

        while (result.next()) {
            counter++;
        }

        if (counter >= 1) {

            App.setRoot("loginScreen");

        }
    }
}
