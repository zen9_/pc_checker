package ru.mycompany.pc_checker;

import java.io.IOException;
import java.sql.SQLException;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import ru.mycompany.pc_checker.dbase.DBHandler;

public class LogInScreenController {

    DBHandler dbHandler = new DBHandler();
    Alert alert = new Alert(Alert.AlertType.INFORMATION);

    @FXML
    private Button addOrderButton;

    @FXML
    private TextField buyerTextField;

    @FXML
    private TextField infoTextField;

    @FXML
    private TextField priceTextField;

    @FXML
    private void switchToPersLoginScreen() throws IOException {
        App.setRoot("personalLogInScreen");
    }

    @FXML
    private void addOrder() throws IOException, ClassNotFoundException, SQLException {

        double priceTextFieldValue = Double.parseDouble(priceTextField.getText());

        dbHandler = new DBHandler();

        dbHandler.addOrderInfo(buyerTextField.getText(), buyerTextField.getText(), priceTextFieldValue);

        buyerTextField.clear();
        buyerTextField.clear();
        priceTextField.clear();

        alert.setTitle("Информация о заказе");
        alert.setHeaderText(null);
        alert.setContentText("Заказ добавлен");
        alert.showAndWait();

    }
}
