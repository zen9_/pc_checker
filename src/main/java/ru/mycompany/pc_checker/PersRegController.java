/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ru.mycompany.pc_checker;

import java.io.IOException;
import java.sql.SQLException;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import ru.mycompany.pc_checker.dbase.DBHandler;
import ru.mycompany.pc_checker.dbase.User;

/**
 *
 * @author zen
 */
public class PersRegController {

    DBHandler dbHandler = new DBHandler();
    Alert alert = new Alert(Alert.AlertType.INFORMATION);

    @FXML
    private Button registrationButton;

    @FXML
    private Button ronMainButton;

    @FXML
    private TextField regLoginText;

    @FXML
    private PasswordField regPasswordText;

    @FXML
    private void switchToPersLoginScreen() throws IOException {
        App.setRoot("personalLogInScreen");
    }

    @FXML
    private void switchToLoginScreen() throws IOException {
        App.setRoot("loginScreen");
    }

    @FXML
    private void signIn() throws IOException, ClassNotFoundException, SQLException {

        signUpNewUser();

    }

    private void signUpNewUser() throws SQLException, ClassNotFoundException {
        dbHandler = new DBHandler();
        alert = new Alert(Alert.AlertType.INFORMATION);

        boolean loginIsEmpty = regLoginText == null || regLoginText.getText().trim().length() == 0;
        boolean passIsEmpty = regPasswordText == null || regPasswordText.getText().trim().length() == 0;

        alert.setTitle("Информация о регистрации");
        alert.setHeaderText(null);

        if (loginIsEmpty || passIsEmpty) {

            alert.setContentText("Информация не заполнена");
            alert.showAndWait();

        } else {

            String login = regLoginText.getText();
            String password = regPasswordText.getText();

            User user = new User(login, password);

            dbHandler.signUpUser(user);

            alert.setContentText("Регистрация прошла успешно");
            alert.showAndWait();

            regLoginText.clear();
            regPasswordText.clear();
        }
    }
}
