/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ru.mycompany.pc_checker.dbase;

/**
 *
 * @author zen
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.ResultSet;

public class DBHandler extends Config {

    Connection dbConnection;
    PreparedStatement prSt;

    public Connection getDbConnection() throws ClassNotFoundException, SQLException {
        String connectionString = "jdbc:mysql://" + dbHost + ":" + dbPort + "/" + dbName;

        Class.forName("com.mysql.jdbc.Driver");
        dbConnection = DriverManager.getConnection(connectionString, dbUser, dbPass);

        return dbConnection;
    }

    public void signUpUser(User user) throws SQLException, ClassNotFoundException {

        String insert
                = "INSERT INTO "
                + Const.USER_TABLE + "("
                + Const.USER_LOGIN + ","
                + Const.USER_PASSWORD + ")"
                + "VALUES(?,?)";

        prSt = getDbConnection().prepareStatement(insert);
        prSt.setString(1, user.getLogin());
        prSt.setString(2, user.getPassword());

        prSt.executeUpdate();
    }

    public void addOrderInfo(String buyer, String info, double price) throws ClassNotFoundException, SQLException {
        String insert
                = "INSERT INTO "
                + Const.ORDER_TABLE + "("
                + Const.ORDER_BUYER + ","
                + Const.ORDER_INFO + ","
                + Const.ORDER_PRICE + ")"
                + "VALUES(?,?,?)";

        prSt = getDbConnection().prepareStatement(insert);
        prSt.setString(1, buyer);
        prSt.setString(2, info);
        prSt.setDouble(3, price);

        prSt.executeUpdate();
    }

    public ResultSet getUser(User user) throws SQLException, ClassNotFoundException {
        ResultSet resSet = null;

        String selectUser
                = "SELECT * FROM "
                + Const.USER_TABLE + " WHERE "
                + Const.USER_LOGIN + "=? AND "
                + Const.USER_PASSWORD + "=?";

        prSt = getDbConnection().prepareStatement(selectUser);
        prSt.setString(1, user.getLogin());
        prSt.setString(2, user.getPassword());

        resSet = prSt.executeQuery();

        return resSet;
    }
}
