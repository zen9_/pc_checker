/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ru.mycompany.pc_checker.dbase;

/**
 *
 * @author zen
 */
public class Const {

    // Таблица пользователей
    public static final String USER_TABLE = "users";
    public static final String USER_ID = "idusers";
    public static final String USER_LOGIN = "login";
    public static final String USER_PASSWORD = "password";

    // Таблица заказов
    public static final String ORDER_TABLE = "orders";
    public static final String ORDER_ID = "idorder";
    public static final String ORDER_BUYER = "buyer";
    public static final String ORDER_INFO = "info";
    public static final String ORDER_PRICE = "price";
}
